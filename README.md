# Credit and debit cards vs temporal cards

Santander bank has received approval for its new "Scratch and win a shopping card" campaign, which consists of a new type of card to be implemented alongside the types they already have.

Meaning:

- Credit card: associated with a checking or fixed account, allows withdrawals of an amount less than or equal to the amount approved by the bank.
- Debit card: associated to a checking account, it allows withdrawing an amount less than or equal to the amount of the associated account.
- Temporal card: associated to a temporary account, allows withdrawals of an amount less than or equal to the amount of the associated account.

Any user or customer can buy or earn a prepaid card associated to a temporary account, the only difference is that when the account reaches 0 it is automatically disabled.

## Output

![Output.png](Output.png)

## Workflow

```
---
title: Sample workflow
---
flowchart LR
start([start])

abstract class Account:
   Atributes --> accountBalance
   Constructor(int accountBalance)
   Methods --> withdrawMoney(int amount)
   
class CurrentAccount --> extends Account
   Constructor(int accountBalance)
   Methods --> withdrawMoney(int amount)
   
   	if amount <= accountBalance {
   	   	accountBalance -= amount }
   	   	
   	else if { print "Insufficient balance" }

class TemporalAccount --> extends Account
   Atributes --> bolean status
   Constructor(int accountBalance)
   Methods --> withdrawMoney(int amount)
     
   	if status is true {
   	   if amount <= accountBalance {
   	   	accountBalance -= amount
   	   	if accountBalance == 0 {
   	   	    status = false
   	   	    print "Account disabled" }
   	   }
   	}
   	
   	else if { print "Insufficient balance"}
   
abstract class Card:
   Atributes --> Account account
   Constructor(Account account)
   Methods --> withdrawMoney(int amount)   
   	       setAccount(Account acount)
   	       
class CreditCard --> extends Card
   Methods(int amount)

class DebittCard --> extends Card
   Methods(int amount)
   
class TemporalCard --> extends Card
   Atributes --> 
       TemporalCard(initial amount): new TemporalAccount(initialAmount)
       Methods(int amount)   

class Main:
     Account account = new CurrentAccount(int accountBalance)
     Card card = new DebitCard() | new CreditCard() | new TemporalCard()
     card.setAccount(acount)
     card.withdrawMoney(amount)
     print "Account balance" & card.accountBalance
     
([end])

```

## Diagram

![Diagram.png](Diagram.png)

## Execution

![Execution.png](Execution.png)

## Tests

![Test1.png](Test1.png)
![Test2.png](Test2.png)
![Test3.png](Test3.png)
![Test4.png](Test4.png)