package university.jala.academic;

public class DebitCard extends Card{
    @Override
    public void withdrawMoney(int amount) {
        account.withdrawMoney(amount);
    }
}
