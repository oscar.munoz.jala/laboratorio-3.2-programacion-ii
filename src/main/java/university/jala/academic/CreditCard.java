package university.jala.academic;

public class CreditCard extends Card{
    @Override
    public void withdrawMoney(int amount) {
        account.withdrawMoney(amount);
    }
}
