package university.jala.academic;

public class CurrentAccount extends Account{
    public CurrentAccount(int accountBalance) {
        super(accountBalance);
    }

    @Override
    public void withdrawMoney(int amount) {
        if (amount <= accountBalance) {
            accountBalance -= amount;
        }
        else {
            System.out.println("Insufficient balance");
        }
    }
}
