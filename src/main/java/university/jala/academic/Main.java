package university.jala.academic;

public class Main {
    public static void main(String[] args) {

        Account c1 = new CurrentAccount(500);
        Card t1 = new DebitCard();
        t1.setAccount(c1);
        t1.withdrawMoney(50);
        System.out.println("Account balance c1: " + c1.accountBalance + "\n");

        Account c2 = new CurrentAccount(100);
        Card t2 = new CreditCard();
        t2.setAccount(c2);
        t2.withdrawMoney(50);
        System.out.println("Account balance c2: " + c2.accountBalance + "\n");

        Card t3 = new TemporalCard(100);
        t3.withdrawMoney(100);
        System.out.println("Account balance t3: " + t3.account.accountBalance);
    }
}
