package university.jala.academic;

abstract class Card {
    protected Account account;

    public void setAccount(Account account) {
        this.account = account;
    }

    public abstract void withdrawMoney(int amount);
}