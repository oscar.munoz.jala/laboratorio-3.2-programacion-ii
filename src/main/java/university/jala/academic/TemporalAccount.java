package university.jala.academic;

public class TemporalAccount extends Account {
    private boolean status;

    public TemporalAccount(int accountBalance) {
        super(accountBalance);
        this.status = true;
    }

    @Override
    public void withdrawMoney(int amount) {
        if (status) {
            if (amount <= accountBalance) {
                accountBalance -= amount;
                if (accountBalance == 0) {
                    status = false;
                    System.out.println("Account disabled");
                }
            }
            else {
                    System.out.println("Insufficient balance");
            }
        }
        else {
            System.out.println("Account disabled");
        }
    }
}
