package university.jala.academic;

public class TemporalCard extends Card{

    public TemporalCard(int initialAmount){
        this.account = new TemporalAccount(initialAmount);
    }
    @Override
    public void withdrawMoney(int amount) {
        account.withdrawMoney(amount);
    }
}
