package university.jala.academic;

abstract class Account {
    protected int accountBalance;

    public Account(int accountBalance) {
        this.accountBalance = accountBalance;
    }

    public abstract void withdrawMoney(int amount);
}
