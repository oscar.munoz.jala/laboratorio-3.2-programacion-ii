package university.jala.academic;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class BankTest {

    @Test
    public void createAccount(){
        Account c1 = new CurrentAccount(100);
        Account c2 = new TemporalAccount(200);
        assertEquals(100,c1.accountBalance);
        assertEquals(200,c2.accountBalance);
    }

    @Test
    public void createCard(){

        Account c1 = new CurrentAccount(100);
        Account c2 = new CurrentAccount(200);

        Card card1 = new DebitCard();
        Card card2 = new CreditCard();
        Card card3 = new TemporalCard(300);

        card1.setAccount(c1);
        card2.setAccount(c2);

        assertEquals(100,card1.account.accountBalance);
        assertEquals(200,card2.account.accountBalance);
        assertEquals(300,card3.account.accountBalance);
    }

    @Test
    public void withdrawAmount(){

        Account c1 = new CurrentAccount(500);
        Card t1 = new CreditCard();
        t1.setAccount(c1);

        Account c2 = new CurrentAccount(100);
        Card t2 = new CreditCard();
        t2.setAccount(c2);

        Card t3 = new TemporalCard(100);
        t3.setAccount(new TemporalAccount(100));
        t3.withdrawMoney(100);

        //assertEquals(50,t1.account.accountBalance); //failed
        //assertEquals(450,t2.account.accountBalance); //failed
        assertEquals(0,t3.account.accountBalance); //Passed

    }

}